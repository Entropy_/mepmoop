This is a set of triggers, scripts and applications for tintin++
and Starmourn the online game.

Currently you have to touch all the *.moop files 
needed. Each .mep file has a matching .moop, so if you want to enable
the default map mep, load by `#read defaultmep.mep` in tt++, then
`tail -f defaultmep.moop` in the window you'd like it to display.
Updating the secondary windows triggers when they receive information
that should be displayed there. Hit F4 or F5 to update all for on ground and
in ship respectively. The hunt modes however don't have to be loaded by
the mep, they're triggered and read once you `mode beast`, and you have
access to them by loading the `#read macros.mep`

You should make a window config that works for you in terminator or what
have you. I find terminator to do the job well. `tail -f -blah-.moop`
for each of the moops you want to follow. Two of the moops are executables,
they're the binaries that live in the mepmoop folder. They'll only work on
Linux systems as they rely on having certain system commands accessible.
They're the targeting system and the statbars, and are interacted with
through the main window. To get the statbars to work, you should A. play
a B.E.A.S.T. and B. change your prompt like so, 

`CONFIG PROMPT NORMAL @%health,@resource1,@%xp,@combat,@balance, | Doot`


To just give you an idea of what the statbars and notifications about
hunt mode look like, see the following.

A colorful picture with a number of lines of status messages and 
punctuation mark-utilizing text bars.
![UIexample.png](UIexample.png)

deps: tt++, sox(for audio), terminator  

install: mv mepmoop/mepmoop/TERMINATOR.config ~/.config/terminator/config  

launch: terminator -p starmourn-main (and for other window) terminator -p starmourn-secondary -l starmourn-secondary-layout  

I recommend tying those two commands to your favourite neglected hotcorner in gnome.  


Enjoy!  

