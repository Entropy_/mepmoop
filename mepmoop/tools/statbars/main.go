package main

import (
	"fmt"
	"strings"
	"strconv"
	"os"
	"os/exec"
	"math"
	"golang.org/x/crypto/ssh/terminal"
)

func main () {

	for {
		width, height, err := terminal.GetSize(int(os.Stdin.Fd()))
		//fmt.Println(width, "WIDTH", height, "HEIGHT")
		if err != nil {
			panic(err)
		}
		prompt := exec.Command("tail", "-n", "1", "prompt.moop")
		line, err := prompt.Output()
		if err != nil {
			panic(err)
		}

		lineString := string(line)
		lineArray := strings.Split(lineString, ",")
		health, err := strconv.Atoi(lineArray[0])
		xp, err := strconv.Atoi(lineArray[2])
		combat := lineArray[3]
		combL := ""
		combatSideBar := "not in combat"
		leftSide := ""
		//halfWidth := int((width/2)-len(combatSideBar))

		for i := 0;i < width-len(combatSideBar);i++ {
			combL += "\033[38:2:150:150:150m-"
			if i == width-len(combatSideBar)-1 {
				leftSide = combL + "\033[0m"
			}
		}
		combR := ""
		//rightSide := int((width/2))
		//for i := 0;i < width;i++ {
		//	combR += "-"
		//	if i == width-1 {
		//		combR = combR
		//	}
		//}
		combatOutput := "\033[0m"+leftSide+"not in combat"+combR+"\033[0m"
		//defaultCombatString := "\033[0m+++++++++++++++++++not in combat+++++++++++++++++++++\033[0m"
		if combat == "C" {
			combatOutput = "\033[0m\033[48:2:200:200:0m"+leftSide+"IN COMBAT"+combR+"\033[0m"

		}else {
			combatOutput = combatOutput
		}
		if err != nil {
			panic(err)
		}
		header := ""
		//guiOutput := "\n\n\n\n\n\033[0;0H\033[0m"
		for i := 1; i <= height;i++ {
			//header += "\n"
			if i == height {
				header = header + "\033[1:1H\033[0m"
				//header += "\n"
			}
		}
		xpOutput := "\033[0m"
		appliedNumber := false
		numOfCellsToFill := (float64(xp)/100)*float64(width)
		numOfCells := (101*float64(100)*math.Pow(float64(width), 2))/(10000*float64(width))
		numOfCells = numOfCells - float64(len(lineArray[2]))
		if numOfCells > 100 {
			numOfCells -= 1
		}
		for w := 1.0;w < numOfCells;w++ {
			if w < numOfCellsToFill - float64(len(lineArray[2])){
				xpOutput += "\033[48:2:200:150:150mX"
			}else if !appliedNumber && w >= numOfCellsToFill - float64(len(lineArray[0])+1) {
				xpOutput += "\033[38:2:10:50:200m"+lineArray[2] + "%"
				appliedNumber = true
			}else {
				xpOutput += "\033[48:2:150:150:150m "

			}
		}
		xpOutput = xpOutput + "\033[0m"

		appliedNumber = false
		numOfCellsToFill = (float64(health)/100)*float64(width)
		numOfCells = (101*float64(100)*math.Pow(float64(width), 2))/(10000*float64(width))
		numOfCells = numOfCells - float64(len(lineArray[0]))
		if numOfCells > 100 {
			numOfCells -= 1
		}
		guiOutput := "\033[0m"
		for w := 1.0;w < numOfCells;w++ {
			if w < numOfCellsToFill - float64(len(lineArray[0])) {
				guiOutput += "\033[48:2:150:150:210m\033[38:2:220:0:0m="
			}else if !appliedNumber && w >= numOfCellsToFill - float64(len(lineArray[0])+1) {
				guiOutput += lineArray[0] + "%"
				appliedNumber = true
			}else {
				guiOutput += "\033[48:2:210:50:50m "
			}
		}
		guiOutput = guiOutput + "\033[0m"






	//FOR PLASMA
		appliedNumber = false
		plasOutput := ""
		plasPercent := 1.0
		plasmaTrigger := "nonelowmediumhighveryhigh"
		plasmaStates := "none,low,medium,high,very high"
		resource1Array := strings.Split(plasmaStates, ",")
		for p := range resource1Array {
			if lineArray[1] == resource1Array[p] {
				plasPercent = float64(len(plasmaStates)) * float64((float64(p)*(100))/100)
			}
		}
		numOfCells = (101*float64(100)*math.Pow(float64(width), 2))/(10000*float64(width))
		numOfCells = numOfCells - float64(len(lineArray[2]))-3
		if numOfCells > 100 {
			numOfCells -= 1
		}
		numOfCellsToFill = (float64(plasPercent)/100)*float64(width)
		if strings.ContainsAny(lineArray[1], plasmaTrigger) {
			for w := 1.0;w < numOfCells;w++ {
				if w < numOfCellsToFill - float64(len(lineArray[1])) {
					plasOutput += "\033[48:2:170:50:50m "
				}else if !appliedNumber && w >= numOfCellsToFill - float64(len(lineArray[1])+1) {
					plasOutput += "\033[38:2:125:25:200m"+lineArray[1]
					appliedNumber = true
				}else {
					plasOutput += "\033[48:2:150:150:210m\033[38:2:220:0:0m="
				}
			}
			plasOutput = plasOutput + "\033[0m"
		}

//		for i:=1;i <= 97;i+=2 {
//			if health >= i {
//				guiOutput += "\033[48:2:0:0:220m "
//			}else {
//				guiOutput += "\033[48:2:120:10:10m "
//			}
//			if i == 97 {
//				guiOutput += "\033[0m}}"
//			}
//		}
		//FOR FUTURE REFERENCE
		//fmt.Println("{{                  PLASMA                         }}")
//		plasmaTrigger := "nonelowmediumhighveryhigh"
//		plasmaStates := "none,low,medium,high,very high"
//		resource1Array := strings.Split(plasmaStates, ",")
//		if strings.ContainsAny(lineArray[1], plasmaTrigger) {
//			plasOutput := ""
//			plasmaLevel := lineArray[1]
//			for i := 0;i < len(resource1Array);i++ {
//				if plasmaLevel == resource1Array[0] {
//					//Add value of state
//					plasOutput = ("{{\033[48:2:235:35:175m\033[5m                  PLASMA                         \033[0m}}")
//
//				}
//				if plasmaLevel == resource1Array[1] {
//					//Add value of state
//					plasOutput = ("{{\033[48:2:235:35:175m          \033[0m                                       }}")
//				}
//				if plasmaLevel == resource1Array[2] {
//					//Add value of state
//					plasOutput = ("{{\033[48:2:235:35:175m                         \033[0m                        }}")
//				}
//				if plasmaLevel == resource1Array[3] {
//					//Add value of state
//					plasOutput = ("{{\033[48:2:235:35:175m                                            \033[0m     }}")
//				}
//				if plasmaLevel == resource1Array[4] {
//					//Add value of state
//					plasOutput = ("{{\033[48:2:235:35:175m\033[5m     DANGER       PLASMA        DANGER           \033[0m}}")
//				}
//			}
			fmt.Println(header+guiOutput+"\n"+xpOutput+"\n"+combatOutput+"\n"+plasOutput)
//			fmt.Println(guiOutput+xpOutput+combatOutput+plasOutput)
//		} else {
//			partOutput := "\033[0m[[\033[48:2:200:15:200m"
//			partLevel := lineArray[1]
//			parts, err := strconv.Atoi(partLevel)
//			if err != nil {
//				panic(err)
//			}
//			for i := 0;i < 48;i++ {
//				if parts >= i {
//					partOutput+=" "
//				} else {
//					partOutput+="\033[0m "
//				}
//			}
//			partOutput += " \033[0m]]\n^^\033[48:2:200:15:200mPARTS\033[0m"
//			fmt.Println(guiOutput+xpOutput+combatOutput+partOutput)
//		}
	}
}
