package main

import (
	"strings"
	"fmt"
	"os/exec"
	"os"
	"io/ioutil"
)

type creature struct {
	Text string
	Selected bool
	Locked bool
}

func main() {

	//Create string tokens with correct color
	//coding

	//piece together a collection of string bits
	//to be used, not including the "space" bit
	//keep for further use
	//Maxlength of mobs should be around 48
	//So let's make the cells 50 long, with
	//Select bits on the end
	//cel := "\033[2:48:150:0:150m+"


	//Initiate selecting varible
	//var sel bool
	var lock bool
	var clearlock bool
	var Locked bool
	creatureArray := make([]creature, 36)
	clearCmd := false
	pos := 0
	for {
             	prompt := exec.Command("tail", "-n", "1", "NPCs.moop")
                lineByte, err := prompt.Output()
                if err != nil {
                        panic(err)
                }
		line := string(lineByte)
		//fmt.Println(line)

		command := exec.Command("tail", "-n", "1", "selcom.moop")
		selcomByte, err := command.Output()
		if err != nil {
			panic(err)
		}
//		rmcommand := exec.Command("rm", "selcom.moop")
//		rmcommand.Run()

		selcom := string(selcomByte)
		selcom = strings.TrimSpace(selcom)
		//fmt.Println(selcom)
		//The codes for use are as follows
		//{w=0} select up
		//{s=1} select down
		//{a=2} select off
		//{d=3 select on
		//{a=4} delock
		//{d=5 lock
		//#macro {5} {#script {doot} {echo "5" > selcom.moop}}


		//Strip the string into an array
		//Strip it on "," because that's
		//happily what the mud sends.
		//Take the last element of the array
		//if the array is longer than 2,
		//and strip out the "and"
		//**ugh strings**
		line = strings.Replace(line, ", and ", ",", -1)
		line = strings.Replace(line, "and ", ",", -1)
		lineArray := strings.Split(line, ",")
		creatureArray = make([]creature, len(lineArray))

		//Set the select bool
		if !Locked {
			if selcom == "0" {
				pos-=1
				if pos < 0 {
					pos = 0
				}
				clearCmd = true
			}
			if selcom == "1" {
				pos+=1
				if pos >= len(lineArray)-1 {
					pos = len(lineArray)-1
				}
				clearCmd = true
			}
			if selcom == "2" {
				//sel\ = false
			}
			if selcom == "3" {
				//sel/ = true
			}
			if selcom == "5" {
				lock = true
			}
		}
		if selcom == "x" {
			//sel\ = false
			clearCmd = false
		}
		if clearCmd {
			clear := exec.Command("bash", "-c", "echo x > selcom.moop")
			clear.Start()
			clear.Wait()
		}
		if selcom == "4" {
			clearlock = true
		}
		fmt.Sprintf(selcom)
		keywordArray := make([]string, len(lineArray))
		//Create and populate the array of structs that
		//holds the creatures. set their selected bool
		for i:=0;i < len(lineArray);i++ {
			creatureArray[i].Text = lineArray[i]

			creatureArray[i].Selected = false
			//Assign the keyword
			//trim the whitespace down to what we want
			creatureArray[i].Text = strings.TrimSpace(creatureArray[i].Text)

			if creatureArray[i].Text != "" {
				words := strings.Split(creatureArray[i].Text, " ")
				wordCopy := words[len(words)-1]
				keywordArray = append(keywordArray, wordCopy)


				cel := fmt.Sprintf("                                                                  ")
				//I am either a genius or just very very stubborn
				celFilled := fmt.Sprintf(cel[:(len(cel)/2)-(len(creatureArray[i].Text)/2)]+creatureArray[i].Text+(cel[(len(cel)/2)+(len(creatureArray[i].Text)/2):]))

				creatureArray[i].Text = celFilled
				//If sel is true, give it the selected color
				b, err := ioutil.ReadFile("locking.moop")
				if err != nil {
					panic(err)
				}
				locking := string(b)
				if lock && i == pos && !Locked {
					creatureArray[i].Locked = true
					//Set the global lock so we aren't able to activate any other locks
					Locked = true
					lockmoop, err := os.Create("lock.moop")
					lockingmoop, err := os.Create("locking.moop")
					if err != nil {
						panic(err)
					}
					targetLock := fmt.Sprintf("#var {COMMON} {"+words[len(words)-1]+"}")
					lockmoop.WriteString(targetLock)
					lockmoop.Sync()
					lockmoop.Close()
					targetLocking := fmt.Sprintf(words[len(words)-1])
					lockingmoop.WriteString(targetLocking)
					lockingmoop.Sync()
					lockingmoop.Close()
					clearCmd = true
					//fmt.Println(words[len(words)-1])
				//	TODO
				}
				if clearlock && Locked {
					for c := 0;c < len(creatureArray);c++ {
						creatureArray[c].Locked = false
					}
					Locked = false
					lock = false
					clearlock = false
					lockmoop, err := os.Create("lock.moop")
					lockingmoop, err := os.Create("locking.moop")
					if err != nil {
						panic(err)
					}
					targetLock := fmt.Sprintf("#var {COMMON} {nothing}")
					lockmoop.WriteString(targetLock)
					lockmoop.Sync()
					lockmoop.Close()
					targetLocking := fmt.Sprintf("nothing")
					lockingmoop.WriteString(targetLocking)
					lockingmoop.Sync()
					lockingmoop.Close()

					clearCmd = true
				}

				if strings.Contains(creatureArray[i].Text, locking) && Locked {
					creatureArray[i].Text = fmt.Sprintln("\033[48:2:255:100:200m  \033[0m "+creatureArray[i].Text[:len(creatureArray[i].Text)-2]+"\033[48:2:255:100:200m  \033[0m")
				} else {
					creatureArray[i].Text = fmt.Sprintln("\033[38:2:200:0:0m{\033[38:2:0:200:200m"+creatureArray[i].Text + "\033[38:2:200:0:0m}\033[0m")
				}

				if  i == pos {
					locking := string(b)
					if strings.Contains(creatureArray[i].Text, locking) && Locked {
						creatureArray[i].Text = fmt.Sprintln("\033[48:2:255:100:200m  "+creatureArray[i].Text[:len(creatureArray[i].Text)-2]+"\033[48:2:255:100:200m  \033[0m")
					} else {
						creatureArray[i].Text = fmt.Sprintf("\033[48:2:100:100:200m[[\033[0m"+creatureArray[i].Text[33:len(creatureArray[i].Text)-23]+"\033[48:2:100:100:200m]]\033[0m\n")
						creatureArray[i].Selected = true

					}

				}
			}

		}

		//Grab the keywords, we're lazy so we'll just
		//make the last word of the description
		//the keyword

		//fmt.Sprintf("%t",sel)
		//Clear the screen
		fmt.Println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n")

		//If sel is true, loop over the creature array and mark the text
		fmt.Printf("\033[0:0H")
		//fmt.Println(keywordArray)
		for _, value := range creatureArray {
			if value.Text != "" {
				//fmt.Printf("%t",value.Locked)
				//fmt.Printf("%t",value.Selected)
				//fmt.Printf(keywordArray[i])
				fmt.Printf(value.Text)
				//creatureArray[i].Text = " "
				//creatureArray[i].Locked = false
			}
		}
		status := ""
		if Locked {
			status = fmt.Sprintln("\033[38:2:200:100:0m[LOCK STATUS: \033[38:2:250:0:0mACTIVE\033[38:2:200:100:0m]\033[0m")
		}else {
			status = fmt.Sprintln("\033[38:2:200:100:0m[LOCK STATUS: \033[38:2:0:0:200mDEACTIVATED\033[38:2:200:100:0m]\033[0m")
		}
		fmt.Printf(status)
//HELPFUL DEBUG LINES
		//fmt.Println(creatureArray)
		//fmt.Println(len(creatureArray))
		//fmt.Println(len(lineArray))
		//fmt.Println(creatureArray[0].Text)
//END HELPFUL DEBUG LINES

		//some selected creature cels
		//{an imposing Redeemer in bright power armor}
		//\033[2:200:0:200m{\033[0m            creature              \033[2:200:0:200m}\033[0m}

		//This can be done OUTSIDE the loop
		//And only initialized once.

		//Take the length of a creature entry
		//and make its length equal 48, fill
		//the rest with whitespace


		//If the cel is selected, apply the correct
		//color code.

	}
}
