package main

import (
	"strconv"
	"fmt"
	"strings"
	"os"
	"bufio"
	"golang.org/x/crypto/ssh/terminal"
	"math"
	"time"
)

func CalculateLine(width int, pos int, contents string, huntOutput string) string {
                appliedNumber := false
                numOfCellsToFill := (float64(pos)/100)*float64(width)
                numOfCells := (101*float64(100)*math.Pow(float64(width), 2))/(10000*float64(width))
                numOfCells = numOfCells - float64(len(contents))
                if numOfCells > 100 {
                        numOfCells -= 1
                }
                for w := 1.0;w <= numOfCells;w++ {
                        if w < numOfCellsToFill{
                                huntOutput += "\033[48:2:0:50:150m\033[38:2:0:0:0m["
                        }else if !appliedNumber && w >= numOfCellsToFill {
                                huntOutput += "\033[38:2:10:150:0m"+contents
                                appliedNumber = true
                        }else {
				huntOutput += "\033[48:2:0:50:150m\033[38:2:0:0:0m]"
			}
                }
		huntOutput = huntOutput + "\033[0m"
		return huntOutput
}

type DamAmts struct {
//   - melee:
	Melee bool
	AmtMelee float64
//   - tech:
	Tech bool
	AmtTech float64
//   - kith:
	Kith bool
	AmtKith float64
//   - ranged:
	Ranged bool
	AmtRanged float64
//   - impact:
	Impact bool
	AmtImpact float64
//   - electromagnetic:
	EM bool
	AmtEM float64
//   - thermal:
	Thermal bool
	AmtThermal float64
//   - cellular:
	Cellular bool
	AmtCellular float64
//   - mental:
	Mental bool
	AmtMental float64
//   - draining:
	Draining bool
	AmtDraining float64
//   - electric:
	Electric bool
	AmtElectric float64

}



type Entry struct {
	Tag string
	First bool
	EngagedTag string
	Count int
	DamageToSlay float64
	DamageTypes string
	DamageAmounts DamAmts
	TimeEngaged time.Time
	TimeSlain time.Time
	Duration time.Duration
}
func NewEntry() Entry {
	var entry Entry
	entry.Tag = ""
	entry.First = false
	entry.EngagedTag = ""
	entry.DamageToSlay = 0.0
	return entry
}
func NewOutputEntry() OutputEntry {
	var entry OutputEntry
	entry.Tag = ""
	entry.First = false
	entry.EngagedTag = ""
	entry.SlayCount = 0
	entry.DamageToSlay = 0.0
	return entry
}
type OutputEntry struct {
	Tag string
	First bool
	EngagedTag string
	SlayCount int
	DamageToSlay float64
	DamageTypes []string
	DamageAmounts DamAmts
	TimeEngaged time.Time
	TimeSlain time.Time
	Duration time.Duration
	AverageDuration time.Duration
}



func main() {

	fmt.Println("This is the statistics tracker for MepMoop")


        huntOutput := "\033[0m"
	filePath := ""
	
	layout := "Mon Jan 02 15:04:05 PDT 2006"

	args := os.Args
	if len(args) > 1 {
		Options:
		for i, options := range args[1:] {
			fmt.Println(options)
			fmt.Println(args)
			switch options {
			case "--load-hunt":
				filePath = args[i+2]
				fmt.Println("Loading hunt : " + filePath)
				break Options
			case "--version":
				fmt.Println("Version 0.1 HuntLog Copyright 2019 Ben Belland")
				os.Exit(1)
			case "-v":
				fmt.Println("Version 0.1 HuntLog Copyright 2019 Ben Belland")
				os.Exit(1)
			case "-h":
				fmt.Println("options -v --version -h --help --new-hunt --load-hunt <old-hunt-moop> --display-marks --display-numbers")
				os.Exit(1)
			case "--help":
				fmt.Println("options -v --version -h --help --new-hunt --load-hunt <old-hunt-moop> --display-marks --display-numbers")
				os.Exit(1)
			default:
				fmt.Println("No flag or incorrect flags given")
				fmt.Println("-h or --help to see usage")
				os.Exit(1)
			}
		}
	}else {
		fmt.Println("No arguments or options specified, -h --help for usage")
		os.Exit(1)
	}
//	for i := 0;i < 1;i++ {
	//For debugging and running once
	
	//Create the table variables
	


	for {
		entryTable := []Entry{}
		width, height, err := terminal.GetSize(int(os.Stdin.Fd()))
	        //fmt.Println(width, "WIDTH", height, "HEIGHT")
	        if err != nil {
	                panic(err)
	        }
		header := ""
//		for i := 0;i < height;i++ {
//			header += "\n"
//		}
	        header += "\033[0:0H"
		file, err := os.Open(filePath)
		if err != nil {
			panic(err)
		}
		//tagLine := ""
		line := ""
		tags := ""
		var timeEng time.Time
		//This is declared outside of the loop to help mitigate false timings
		scanner := bufio.NewScanner(file)
		time.Sleep(1*time.Second)
		entry := NewEntry()
		//Compile the table
		for scanner.Scan() {
			//fmt.Println(scanner.Text())
			switch scanner.Text() {
			case "tag":
				scanner.Scan()
				contents := scanner.Text()
				if !strings.Contains(tags, contents) {
					tags += ","+contents
					entry.First = true
				}
				entry.Tag = contents

			case "engagetime":
				scanner.Scan()

				timeEng, err = time.Parse(layout, scanner.Text())
				if err != nil {
					panic(err)
				}
				entry.TimeEngaged = timeEng
				//contents := "Engaged: " + timeEng.String()
				line += fmt.Sprint("Engaged:")
				//line += fmt.Sprint("Engaged:"+scanner.Text())
			case "dam":
				scanner.Scan()
				//todo
				amountI, err := strconv.Atoi(scanner.Text())
				if err != nil {
					panic(err)
				}
				amount := float64(amountI)
				damageTypes := strings.Split(entry.DamageTypes, ",")
				for i := 0;i < len(damageTypes);i++ {
					switch damageTypes[i] {
					case "melee":
						entry.DamageAmounts.Melee = true
						entry.DamageAmounts.AmtMelee += amount
					case "tech":
						entry.DamageAmounts.Tech = true
						entry.DamageAmounts.AmtTech += amount
					case "kith":
						entry.DamageAmounts.Kith = true
						entry.DamageAmounts.AmtKith += amount
					case "ranged":
						entry.DamageAmounts.Ranged = true
						entry.DamageAmounts.AmtRanged += amount
					case "impact":
						entry.DamageAmounts.Impact = true
						entry.DamageAmounts.AmtImpact += amount
					case "electromagnetic":
						entry.DamageAmounts.EM = true
						entry.DamageAmounts.AmtEM += amount
					case "thermal":
						entry.DamageAmounts.Thermal = true
						entry.DamageAmounts.AmtThermal += amount
					case "cellular":
						entry.DamageAmounts.Cellular = true
						entry.DamageAmounts.AmtCellular += amount
					case "mental":
						entry.DamageAmounts.Mental = true
						entry.DamageAmounts.AmtMental += amount
					case "draining":
						entry.DamageAmounts.Draining = true
						entry.DamageAmounts.AmtDraining += amount
					case "electric":
						entry.DamageAmounts.Electric = true
						entry.DamageAmounts.AmtElectric += amount
					}
				}
				line += fmt.Sprint("Amount:")
				//line += fmt.Sprint("Amount:"+scanner.Text())
			case "dtype":
				scanner.Scan()
				entry.DamageTypes += scanner.Text()+","
				line += fmt.Sprint("Type:")
				damageTypes := strings.Split(entry.DamageTypes, ",")
				for i := 0;i < len(damageTypes);i++ {
					switch damageTypes[i] {
					case "melee":
						entry.DamageAmounts.Melee = true
					case "tech":
						entry.DamageAmounts.Tech = true
					case "kith":
						entry.DamageAmounts.Kith = true
					case "ranged":
						entry.DamageAmounts.Ranged = true
					case "impact":
						entry.DamageAmounts.Impact = true
					case "electromagnetic":
						entry.DamageAmounts.EM = true
					case "thermal":
						entry.DamageAmounts.Thermal = true
					case "cellular":
						entry.DamageAmounts.Cellular = true
					case "mental":
						entry.DamageAmounts.Mental = true
					case "draining":
						entry.DamageAmounts.Draining = true
					case "electric":
						entry.DamageAmounts.Electric = true
					}
				}
				//line += fmt.Sprint("Type:"+scanner.Text())
			case "slaintime":
				scanner.Scan()
				line += fmt.Sprint("Slain:")
				slainTime, err := time.Parse(layout, scanner.Text())
				if err != nil {
					panic(err)
				}
				entry.TimeSlain = slainTime
				entry.Duration = entry.TimeSlain.Sub(entry.TimeEngaged)

				//line += fmt.Sprint("Slain:"+scanner.Text())
			default:
				fmt.Sprint("Triggers on SLAIN")
				entryTable = append(entryTable, entry)
				entry = NewEntry()

			}
		}
		file.Close()
		var tagArray []string
		var outputTable []OutputEntry
		//Declare variables for output
		//fmt.Println(entryTable)
		for e := range entryTable {
			if entryTable[e].First == true {
				output := NewOutputEntry()
				output.Tag = entryTable[e].Tag
				tagArray = append(tagArray, entryTable[e].Tag)
				outputTable = append(outputTable, output)
			}
		}

		//Now process
		for e, ent := range entryTable {
			for i := range tagArray {

				if ent.Tag == tagArray[i] {
					//Do stuff TODO
					//Compile full damage
					if ent.DamageAmounts.Melee  {
						outputTable[i].DamageAmounts.AmtMelee += ent.DamageAmounts.AmtMelee
					}
					if ent.DamageAmounts.Tech  {
						outputTable[i].DamageAmounts.AmtTech += ent.DamageAmounts.AmtTech
					}
					if ent.DamageAmounts.Kith  {
						outputTable[i].DamageAmounts.AmtKith += ent.DamageAmounts.AmtKith
					}
					if ent.DamageAmounts.Ranged  {
						outputTable[i].DamageAmounts.AmtRanged += ent.DamageAmounts.AmtRanged
					}
					if ent.DamageAmounts.Impact  {
						outputTable[i].DamageAmounts.AmtImpact += ent.DamageAmounts.AmtImpact
					}
					if ent.DamageAmounts.EM  {
						outputTable[i].DamageAmounts.AmtEM += ent.DamageAmounts.AmtEM
					}
					if ent.DamageAmounts.Thermal  {
						outputTable[i].DamageAmounts.AmtThermal += ent.DamageAmounts.AmtThermal
					}
					if ent.DamageAmounts.Cellular  {
						outputTable[i].DamageAmounts.AmtCellular += ent.DamageAmounts.AmtCellular
					}
					if ent.DamageAmounts.Mental  {
						outputTable[i].DamageAmounts.AmtMental += ent.DamageAmounts.AmtMental
					}
					if ent.DamageAmounts.Draining  {
						outputTable[i].DamageAmounts.AmtDraining += ent.DamageAmounts.AmtDraining
					}
					if ent.DamageAmounts.Electric  {
						outputTable[i].DamageAmounts.AmtElectric += ent.DamageAmounts.AmtElectric
					}

					//calculate time duration
					outputTable[i].Duration += ent.Duration

					//calculate notches
					outputTable[i].SlayCount++
				}

			if e == len(entryTable) - 1 {
				outputTable[i].AverageDuration = outputTable[i].Duration / time.Duration(outputTable[i].SlayCount)
			}
		}
		
		
	}
	fmt.Print(header)

	for _, out := range outputTable {
		line := CalculateLine(width, 10, out.Tag, huntOutput)
		fmt.Println(line)
		slayCountString := strconv.Itoa(out.SlayCount)
		aveDurString := out.AverageDuration.String()
		line = CalculateLine(width, 10, slayCountString+" Notches::"+aveDurString, huntOutput)
		fmt.Println(line)
	}


	//calculate average time
	//output.AverageDuration = output.Duration 

	fmt.Sprint(huntOutput)
	//format the output
	fmt.Sprintln("\033[0m\nWIDTH:",width,"\n","HEIGHT:",height)

	//line = CalculateLine(width, 5, line, huntOutput)
	//fmt.Println(tagLine)
	//fmt.Println(line)
	//fmt.Println(dim)
	}

}
